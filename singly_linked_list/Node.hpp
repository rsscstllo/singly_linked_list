//
//  Node.hpp
//  singly_linked_list
//
//  Created by Ross Castillo on 11/15/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

struct Node {
public:
  // Constructor
  Node(int data, Node* next);
  
  // Getters
  int get_data() const;
  Node* get_next() const;
  
  // Setters
  void set_data(int data);
  void set_next(Node* next);
  
  // Destructor
  ~Node(){};
  
private:
  int data_;
  Node* next_;
};

#endif /* Node_hpp */
