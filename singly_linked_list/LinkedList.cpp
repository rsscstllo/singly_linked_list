//
//  LinkedList.cpp
//  singly_linked_list
//
//  Created by Ross Castillo on 11/15/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

#include "LinkedList.hpp"

LinkedList::LinkedList() {
  head_ = nullptr;
}

Node* LinkedList::get_head() const {
  return head_;
}

void LinkedList::set_head(Node* head) {
  head_ = head;
}

#pragma mark - Insert

void LinkedList::InsertNodeAtBeginning(int data) {
  // Create a new node whose next node is the current head node
  Node* head = new Node(data, head_);
  // Then set the current head of the list to now be this new node
  head_ = head;
}

void LinkedList::InsertNodeAtEnd(int data) {
  // If the list is empty, set the head to the new node being added
  if (head_ == nullptr)
    head_ = new Node(data, nullptr);
  
  // If the list has one element, set the head's next node to the new node
  else if (head_->get_next() == nullptr)
    head_->set_next(new Node(data, nullptr));
  
  // If the list has two or more elements, iterate through to set the last node
  else {
    Node* head = head_->get_next();
    
    // Iterate through the list until one from the end
    while (head->get_next() != nullptr)
      head = head->get_next();
    
    head->set_next(new Node(data, nullptr));
  }
}

void LinkedList::InsertNodeAt(int data, int position) {
  // If inserting into the first position, add to the beginning of the list
  if (position == 0)
    InsertNodeAtBeginning(data);
  
  // If inserting into a position other than the first
  else {
    Node* head = head_;
    Node* new_node = new Node(data, nullptr);
    
    // - 2 because starting at i = 0 and head is the first element
    for (int i = 0; i < position - 2; i++)
      head = head->get_next();
    
    // Set the the new node's next node to be the current node's next node
    new_node->set_next(head->get_next());
    // Set the current node's next node to be the new node just added
    head->set_next(new_node);
  }
}

#pragma mark - Delete

void LinkedList::DeleteNodeAtBeginning() {
  if (head_ != nullptr) {
    Node* head = head_;
    head_ = head->get_next();  // Point head to the second node in the list
    delete head;
  }
}

void LinkedList::DeleteNodeAtEnd() {
  if (head_ != nullptr) {
    
    // If only one item in the list
    if (head_->get_next() == nullptr) {
      DeleteNodeAtBeginning();
      
    } else {
      Node* head = head_;
      
      while (head->get_next()->get_next() != nullptr)
        head = head->get_next();
      
      delete head->get_next();
      head->set_next(nullptr);
    }
  }
}

void LinkedList::DeleteNodeAt(int position) {
  if (head_ != nullptr) {
    if (position == 0) {
      DeleteNodeAtBeginning();
      
    } else {
      Node* head = head_;
      
      for (int i = 0; i < position - 2; i++)
        // Move the head to the (n-1)th node
        head = head->get_next();
      
      // Set temp_node to nth node
      Node* temp_node = head->get_next();
      // Point head to (n+1)th node
      head->set_next(temp_node->get_next());
      // Delete the nth node
      delete temp_node;
    }
  }
}

void LinkedList::DeleteAllNodes() {
  if (head_ != nullptr) {
    // Delete everything after the head
    Node* head = head_->get_next();
    // Still need to keep the head of the list
    head_ = nullptr;
    // Iterate through each node
    while (head != nullptr) {
      Node* next_node = head->get_next();
      delete head;
      head = next_node;
    }
  }
}

#pragma mark - Get Values

int LinkedList::GetNodeValueAtBeginning() {
  if (head_ == nullptr)
    return -1;
  
  else
    return head_->get_data();
}

int LinkedList::GetNodeValueAtEnd() {
  if (head_ == nullptr)
    return -1;
  
  else if (head_->get_next() == nullptr)
    return head_->get_data();
  
  else {
    Node* head = head_;
    while (head->get_next() != nullptr)
      head = head->get_next();
    return head->get_data();
  }
}

int LinkedList::GetNodeValueAt(int position) {
  if (head_ == nullptr)
    return 0;
  
  else if (position == 0 && head_ != nullptr)
    return head_->get_data();
  
  else {
    Node* head = head_;
    while (position--) {
      if (head_->get_next() == nullptr)
        std::cout << "List is not that long!\n";
      head = head->get_next();
    }
    return head->get_data();
  }
}

int LinkedList::GetListLength() {
  if (head_ == nullptr)
    return 0;
  
  else {
    int length = 0;
    Node* head = head_;
    while (head != nullptr) {
      ++length;
      head = head->get_next();
    }
    return length;
  }
}

#pragma mark - Search

bool LinkedList::SearchValue(int data) {
  if (head_ == nullptr)
    return false;
  
  else if (head_->get_data() == data)
    return true;
  
  else {
    Node* head = head_;
    while (head->get_next() != nullptr) {
      if (head->get_data() == data)
        return true;
      head = head->get_next();
    }
    return false;
  }
}

#pragma mark - Reverse List

void LinkedList::ReverseListIteratively() {
  Node* previous = nullptr;
  Node* current = head_;
  Node* next = nullptr;
  
  // Iterate through list until
  while (current != nullptr) {
    std::cout << current->get_data() << "-";
    
    next = current->get_next();
    current->set_next(previous);
    previous = current;
    current = next;
  }
  current = previous;
}

void LinkedList::ReverseListRecursively(Node* head) {
  // Base case
  if (head->get_next() == nullptr) {
    head_ = head;
    return;
  }
  // Recursive step
  ReverseListRecursively(head->get_next());
  
  // Called when recursion un-winding and traversing list backwards
  // Example: 1 2 3 4 5
  // head is 4 on the way back
  // temp = 5, point 5 to 4, point 4 to null
  Node* temp = head->get_next();
  temp->set_next(head);
  head->set_next(nullptr);
}

#pragma mark - Print

void LinkedList::PrintListForwardIteratively() {
  if (head_ != nullptr) {
    Node* head = head_;
    while (head != nullptr) {
      std::cout << head->get_data() << " ";
      head = head->get_next();
    }
  } else {
    std::cout << "List is empty!";
  }
  std::cout << std::endl;
}

// Iterative much more efficient
void LinkedList::PrintListForwardRecursively(Node* head) {
  if (head == nullptr) return;  // Base case
  std::cout << head->get_data() << " ";
  PrintListForwardRecursively(head->get_next());  // Recursive step
}

// Prints out elements upon recursive steps un-winding
void LinkedList::PrintListReverseRecursively(Node* head) {
  if (head == nullptr) return;  // Base case
  PrintListReverseRecursively(head->get_next());  // Recursive step
  std::cout << head->get_data() << " ";
}

#pragma mark - Deconstructor

// Iterate through the list and delete all Nodes
LinkedList::~LinkedList() {
  Node* head = head_;
  while (head != nullptr) {
    Node* next_node = head->get_next();
    delete head;
    head = next_node;
  }
}
