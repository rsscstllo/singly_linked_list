//
//  Node.cpp
//  singly_linked_list
//
//  Created by Ross Castillo on 11/15/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

#include "Node.hpp"

Node::Node(int data, Node* next) {
  data_ = data;
  next_ = next;
}

int Node::get_data() const {
  return data_;
}

Node* Node::get_next() const {
  return next_;
}

void Node::set_data(int data) {
  data_ = data;
}

void Node::set_next(Node* next) {
  next_ = next;
}
