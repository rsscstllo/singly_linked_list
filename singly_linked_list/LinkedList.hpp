//
//  LinkedList.hpp
//  singly_linked_list
//
//  Created by Ross Castillo on 11/15/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <iostream>
#include "Node.hpp"

struct LinkedList {
public:
  // Constructor
  LinkedList();
  
  // Getter and Setter
  Node* get_head() const;
  void set_head(Node* head);
  
  // Insert Nodes
  void InsertNodeAtBeginning(int data);
  void InsertNodeAtEnd(int data);
  void InsertNodeAt(int data, int position);
  
  // Delete Nodes
  void DeleteNodeAtBeginning();
  void DeleteNodeAtEnd();
  void DeleteNodeAt(int position);
  void DeleteAllNodes();
  
  // Get Values
  int GetNodeValueAtBeginning();
  int GetNodeValueAtEnd();
  int GetNodeValueAt(int position);
  int GetListLength();
  
  // Search Value
  bool SearchValue(int data);
  
  // Reverse List
  void ReverseListIteratively();
  void ReverseListRecursively(Node* head);
  
  // Print List
  void PrintListForwardIteratively();
  void PrintListForwardRecursively(Node* head);
  void PrintListReverseRecursively(Node* head);
  
  // Destructor
  ~LinkedList();
  
private:
  Node* head_;
};

#endif /* LinkedList_hpp */
