//
//  main.cpp
//  singly_linked_list
//
//  Created by Ross Castillo on 11/15/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

#include <iostream>
#include "LinkedList.hpp"

int main() {
  LinkedList linked_list;
  
//  linked_list.InsertNodeAtBeginning(1);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAtBeginning(2);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAtBeginning(3);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAtBeginning(4);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAtBeginning(5);
//  linked_list.PrintListForwardIteratively();
  
  linked_list.InsertNodeAtEnd(1);
  linked_list.PrintListForwardIteratively();
  linked_list.InsertNodeAtEnd(2);
  linked_list.PrintListForwardIteratively();
  linked_list.InsertNodeAtEnd(3);
  linked_list.PrintListForwardIteratively();
  linked_list.InsertNodeAtEnd(4);
  linked_list.PrintListForwardIteratively();
  linked_list.InsertNodeAtEnd(5);
  linked_list.PrintListForwardIteratively();
  
//  linked_list.InsertNodeAt(2, 0);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAt(3, 1);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAt(4, 0);
//  linked_list.PrintListForwardIteratively();
//  linked_list.InsertNodeAt(5, 1);
//  linked_list.PrintListForwardIteratively();
  
//  linked_list.DeleteNodeAt(4);
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtBeginning();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtBeginning();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtBeginning();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtBeginning();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtEnd();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtEnd();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtEnd();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtEnd();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteNodeAtEnd();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.DeleteAllNodes();
//  linked_list.PrintListForwardIteratively();
//  
//  linked_list.InsertNodeAtEnd(3);
//  linked_list.PrintListForwardIteratively();
  
  std::cout << std::endl;
  std::cout << "Node value at beginning of list: " << linked_list.GetNodeValueAtBeginning() << std::endl;
  std::cout << "Node value at end of list: " << linked_list.GetNodeValueAtEnd() << std::endl;
  std::cout << "Node value at 3rd position of list is: " << linked_list.GetNodeValueAt(3) << std::endl;
  std::cout << "Length of list: " << linked_list.GetListLength() << std::endl;
  std::cout << "Search for 3: " << linked_list.SearchValue(3) << std::endl;
  std::cout << "Search for 12: " << linked_list.SearchValue(12) << std::endl << std::endl << std::endl;
  
  std::cout << "Reversed list iteratively: ";
  linked_list.ReverseListIteratively();
  linked_list.PrintListForwardIteratively();
  
  std::cout << "Reversed list recursively: ";
  linked_list.ReverseListRecursively(linked_list.get_head());
  linked_list.PrintListForwardIteratively();
  std::cout << std::endl;
  
  std::cout << "Printed forward iteratively: ";
  linked_list.PrintListForwardIteratively();
  std::cout << "Printed forward recursively: ";
  linked_list.PrintListForwardRecursively(linked_list.get_head());
  std::cout << std::endl << "Printed reverse recursively: ";
  linked_list.PrintListReverseRecursively(linked_list.get_head());
  std::cout << std::endl;
  
  return 0;
}
